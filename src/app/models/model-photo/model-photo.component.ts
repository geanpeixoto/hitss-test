import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';

@Component({
  selector: 'app-model-photo',
  templateUrl: './model-photo.component.html',
  styleUrls: ['./model-photo.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    'class': 'app-model-photo'
  }
})
export class ModelPhotoComponent {

  private invalid: boolean;

  private _src: string;

  @Input() set src(src: string) {
    this._src = src;
    this.invalid = !src;
  }

  get src(): string {
    return this._src;
  }

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) { }

  hasErrors() {
    return this.invalid;
  }

  handleError() {
    this.invalid = true;
    this.changeDetectorRef.markForCheck();
  }
}
