import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ImmutableModel, Model } from '../shared/model';
import { ModelsService } from '../shared/models.service';

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false
})
export class ModelsComponent implements OnInit {

  models: ImmutableModel[];

  constructor(
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly modelsService: ModelsService) { }

  create() {
    this.models = [new ImmutableModel({}), ...this.models];
  }

  ngOnInit() {
    this.modelsService.list()
      .subscribe(models => {
        this.models = models;
        this.changeDetectorRef.markForCheck();
      });
  }

  handleModelChanges(newer: ImmutableModel, older: ImmutableModel) {
    if (!newer) {
      // O registro foi removido;
      this.models = this.models.filter(m => m !== older);
    } else {
      // O registrou foi editado;
      this.models = this.models.map(m => m === older ? newer : m);
    }
    this.changeDetectorRef.markForCheck();
  }
}
