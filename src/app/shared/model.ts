import { immutable } from '../core/immutable';

/**
 * Modelo de celular
 */
export class Model {
  code: string;
  model: string;
  price: number;
  brand: string;
  /**
   * Url em que a foto está hospedada
   */
  photo: string;
  date: Date;
}

export class ImmutableModel extends immutable(Model) { }
