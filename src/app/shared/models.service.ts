import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ImmutableModel, Model } from './model';
import { ModelsRepository } from './models.repository';

@Injectable()
export class ModelsService {

  constructor(private readonly repository: ModelsRepository) { }

  delete(model: ImmutableModel): Observable<void> {
    return this.repository.delete(model);
  }

  /**
   * Persiste um registro, quando é um novo registro cria-o, do contrário
   * ele atualiza o registro existênte.
   */
  save(model: ImmutableModel): Observable<ImmutableModel> {
    return !model.code ? this.create(model) : this.update(model);
  }

  /**
   * Cria um novo @see Model
   */
  create(model: ImmutableModel): Observable<ImmutableModel> {
    return this.repository.create(model)
      .pipe(map(m => new ImmutableModel(m)));
  }

  /**
   * Atualiza um registo de @see Model existente
   */
  update(model: ImmutableModel): Observable<ImmutableModel> {
    return this.repository.update(model)
      .pipe(map(m => new ImmutableModel(m)));
  }

  list(): Observable<ImmutableModel[]> {
    return this.repository.list()
      .pipe(map(models => models.map(model => new ImmutableModel(model))));
  }
}
