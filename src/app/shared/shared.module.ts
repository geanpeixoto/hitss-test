import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModelsRepository } from './models.repository';
import { ModelsService } from './models.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ModelsService,
    ModelsRepository,
  ]
})
export class SharedModule { }
