export type Constructor<T> = new (...args: any[]) => T;

export type EmptyConstructor<T> = new () => T;
